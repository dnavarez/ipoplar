//
//  AppError.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/25/22.
//

import Foundation

enum AppError: Error {
  case unknown
  
  var value: String {
    switch self {
    case .unknown:
      return "An error has encountred! Please try again later."
    }
  }
}
