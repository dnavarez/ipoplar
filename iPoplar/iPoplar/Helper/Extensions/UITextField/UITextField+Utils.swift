//
//  UITextField+Utils.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/26/22.
//

import UIKit
import RxSwift

extension UITextField {
  
  func rxBindTextField(to subject: BehaviorSubject<String>
  ) -> Disposable {
    self.rx.text.orEmpty.asDriver().drive(subject)
  }
  
}
