//
//  String+Utils.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/26/22.
//

import Foundation

extension String {
  
  /// Returns a new string made by removing whitespaces from both ends.
  var trimmed: String { trimmingCharacters(in: .whitespaces) }
  
  func isNumeric() -> Bool {
    guard let _ = Double(self) else { return false }
    return true
  }
  
  func doubleValue() -> Double {
    guard let d = Double(self) else { return 0.0 }
    return d
  }
  
  func intValue() -> Int {
    guard let i = Int(self) else { return 0 }
    return i
  }
  
}
