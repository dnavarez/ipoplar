//
//  PropertyCellViewModel.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/25/22.
//

import UIKit

protocol PropertyCellViewModelProtocol: AnyObject {
  
  var imageUrl: String { get }
  var detail: String { get }
  var discountedPrice: String { get }
  var price: String { get }
  var address: String { get }
  
}

class PropertyCellViewModel: PropertyCellViewModelProtocol {
  
  let propertyUnit: PropertyUnit
  
  init(propertyUnit: PropertyUnit) {
    self.propertyUnit = propertyUnit
  }
  
}

// MARK: Getter

extension PropertyCellViewModel {
  
  var imageUrl: String {
    propertyUnit.imageUrl
  }
  
  var detail: String {
    "\(propertyUnit.beds)-bedroom Apartment"
  }
  
  var discountedPrice: String {
    // Dummy just to show a discounted price
    "$\(propertyUnit.rent - 200)"
  }
  
  var price: String {
    "$\(propertyUnit.rent)"
  }
  
  var address: String {
    propertyUnit.address
  }
  
}
