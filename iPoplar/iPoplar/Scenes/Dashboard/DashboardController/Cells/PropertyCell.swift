//
//  PropertyCell.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/25/22.
//

import UIKit
import SDWebImage

class PropertyCell: UITableViewCell {
  
  var viewModel: PropertyCellViewModelProtocol! {
    didSet {
      update()
    }
  }
  
  @IBOutlet weak var propertyImageView: UIImageView!
  @IBOutlet weak var detailLabel: UILabel!
  @IBOutlet weak var discountedPriceLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setup()
  }
  
  func update() {
    detailLabel.text = viewModel.detail
    detailLabel.textColor = .gray
    discountedPriceLabel.text = viewModel.discountedPrice
    addressLabel.text = viewModel.address
    addressLabel.textColor = .gray
    
    let attrString =  NSMutableAttributedString(string: viewModel.price)
    attrString.addAttribute(
      NSAttributedString.Key.strikethroughStyle,
      value: 1,
      range: NSMakeRange(0, attrString.length)
    )
    attrString.addAttribute(
      NSAttributedString.Key.strikethroughColor,
      value: UIColor.red,
      range:NSMakeRange(0, attrString.length)
    )
    priceLabel.attributedText = attrString
    
    let imageUrl = URL(string: viewModel.imageUrl)
    propertyImageView.sd_setImage(with: imageUrl)
    propertyImageView.layer.cornerRadius = 5
  }
  
}

// MARK: - Setup

extension PropertyCell {
  
  func setup() {
    detailLabel.font = .systemFont(ofSize: 16)
    discountedPriceLabel.font = .boldSystemFont(ofSize: 28)
    discountedPriceLabel.textColor = .red
    priceLabel.font = .boldSystemFont(ofSize: 20)
    addressLabel.font = .systemFont(ofSize: 18)
    
    propertyImageView.contentMode = .scaleAspectFill
    selectionStyle = .none
  }
  
}
