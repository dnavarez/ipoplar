//
//  DashboardController.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/25/22.
//

import UIKit

import RxCocoa
import RxSwift
import NSObject_Rx
import SVProgressHUD

class DashboardController: UIViewController {
  
  // Public Properties
  var viewModel: DashboardViewModelProtocol!
  
  // IBOutlet
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var tableView: UITableView!
  
  // Private Properties
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setups()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    viewModel.loadProperty()
    tableView.reloadData()
  }
  
}

// MARK: - Setups

private extension DashboardController {
  
  func setups() {
    setupNavigation()
    setupTableView()
    setupBindings()
  }
  
  func setupNavigation() {
    navigationItem.title = viewModel.title
    
    navigationItem.rightBarButtonItem = UIBarButtonItem(
      barButtonSystemItem: .add,
      target: self,
      action: #selector(moreButtonTapped)
    )
  }
  
  func setupTableView() {
    tableView.dataSource = self

    tableView.register(
      UINib(resource: R.nib.propertyCell),
      forCellReuseIdentifier: R.nib.propertyCell.name
    )
  }
  
  func setupBindings() {
    viewModel.contentState
      .subscribe(onNext: { [unowned self] state in
        self.handleContentStateChanges(state)
      })
      .disposed(by: rx.disposeBag)
  }
  
}

// MARK: - Methods

private extension DashboardController {
  
}

// MARK: - Events

private extension DashboardController {
  
  @objc func moreButtonTapped() {
    guard let vc = R.storyboard.dashboard.newPropertyController() else { return }
    vc.viewModel = NewPropertyControllerViewModel()
    vc.viewModel.delegate = self
    vc.modalPresentationStyle = .overCurrentContext
    present(vc, animated: true)
  }
  
}

// MARK: - Content State

private extension DashboardController {
  
  private func handleContentStateChanges(_ state: ContentState) {
    switch state {
    case .empty:
      tableView.reloadData()
    case .loading:
      SVProgressHUD.show()
    case let .error(e):
      SVProgressHUD.showError(withStatus: e.localizedDescription)
    case .ready:
      tableView.reloadData()
      SVProgressHUD.dismiss()
    }
  }
  
}

// MARK: - UITableViewDataSource

extension DashboardController: UITableViewDataSource {
  
  func tableView(
    _ tableView: UITableView,
    numberOfRowsInSection section: Int
  ) -> Int {
    return viewModel.propertyUnit.count
  }
  
  func tableView(
    _ tableView: UITableView,
    cellForRowAt indexPath: IndexPath
  ) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(
      withIdentifier: R.nib.propertyCell.name
    ) as? PropertyCell
    else {
      return UITableViewCell()
    }

    let property = viewModel.propertyUnit[indexPath.row]
    cell.viewModel = PropertyCellViewModel(propertyUnit: property)

    return cell
  }
  
}

// MARK: - NewPropertyControllerViewModelDelegate

extension DashboardController: NewPropertyControllerViewModelDelegate {
  
  func onSuccess() {
    viewModel.loadProperty()
  }
  
}

// MARK: - Presenter

extension DashboardController {
  
  // Just a sample. Replace with View Controller
  func presentAddNewProperty() {
    let alertVC = UIAlertController(
      title: nil,
      message: "Add New Property",
      preferredStyle: .alert
    )
    
    addTextField(alertVC, placeholder: "Address")
    addTextField(alertVC, placeholder: "Baths")
    addTextField(alertVC, placeholder: "Beds")
    addTextField(alertVC, placeholder: "Rent")
    
    alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel))
    alertVC.addAction(UIAlertAction(title: "Submit", style: .cancel))
    
    present(alertVC, animated: true)
  }
  
  func addTextField(_ alertVC: UIAlertController, placeholder: String) {
    alertVC.addTextField { textField in
      textField.placeholder = placeholder
    }
  }
  
}
