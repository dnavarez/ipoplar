//
//  DashboardViewModel.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/25/22.
//

import NSObject_Rx
import RxRelay
import RxSwift

protocol DashboardViewModelProtocol: AnyObject {
  
  var title: String { get }
  var propertyUnit: [PropertyUnit] { get }
  var contentState: BehaviorRelay<ContentState> { get }
  
  func loadProperty()
  
}

class DashboardViewModel: DashboardViewModelProtocol {
  
  let contentState = BehaviorRelay<ContentState>(value: .empty)
  
  var propertyUnit: [PropertyUnit] = []
  
  init() {
  }
  
}

// MARK: - Fetch API

extension DashboardViewModel {
  
  func loadProperty() {
    contentState.accept(.loading)
    
    APIClient().getBuildings { buildings in
      self.propertyUnit = buildings
      self.contentState.accept(.ready)
    } failure: { error in
      self.contentState.accept(.error(AppError.unknown))
    }

  }
  
}

// MARK: - Getter

extension DashboardViewModel {
  
  var title: String {
    "Poplar"
  }
  
}
