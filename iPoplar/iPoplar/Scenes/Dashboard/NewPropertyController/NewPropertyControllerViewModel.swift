//
//  NewPropertyControllerViewModel.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/26/22.
//

import NSObject_Rx
import RxRelay
import RxSwift

protocol NewPropertyControllerViewModelDelegate: AnyObject {
  
  func onSuccess()
  
}

protocol NewPropertyControllerViewModelProtocol: AnyObject {
  
  var delegate: NewPropertyControllerViewModelDelegate? { get set }
  
  var addressPlaceHolder: String { get }
  var bathsPlaceHolder: String { get }
  var bedsPlaceHolder: String { get }
  var rentPlaceHolder: String { get }
  
  var address: BehaviorSubject<String> { get }
  var baths: BehaviorSubject<String> { get }
  var beds: BehaviorSubject<String> { get }
  var rent: BehaviorSubject<String> { get }
  
  func postNewProperty(success: @escaping () -> Void, failure: @escaping failure)
  
}

class NewPropertyControllerViewModel: NewPropertyControllerViewModelProtocol {
  
  weak var delegate: NewPropertyControllerViewModelDelegate?
  
  let address: BehaviorSubject<String>
  let baths: BehaviorSubject<String>
  let beds: BehaviorSubject<String>
  let rent: BehaviorSubject<String>
  
  init() {
    address = BehaviorSubject<String>(value: "")
    baths = BehaviorSubject<String>(value: "")
    beds = BehaviorSubject<String>(value: "")
    rent = BehaviorSubject<String>(value: "")
  }
  
}

// MARK: - API

extension NewPropertyControllerViewModel {
  
  func postNewProperty(
    success: @escaping () -> Void,
    failure: @escaping failure
  ) {
    APIClient().postNewPropertyUnit(
      address: addressValue,
      baths: bathsValue,
      beds: bedsValue,
      rent: rentValue
    ) { [weak self] in
      self?.delegate?.onSuccess()
      success()
    } failure: { error in
      failure(error)
    }
  }
  
}

// MARK: - Getter

extension NewPropertyControllerViewModel {
  
  var addressPlaceHolder: String { "Address" }
  var bathsPlaceHolder: String { "Number of baths" }
  var bedsPlaceHolder: String { "Number of beds" }
  var rentPlaceHolder: String { "Price" }
  
  var addressValue: String {
    return (try? address.value().trimmed) ?? ""
  }
  var bathsValue: String {
    return (try? baths.value().trimmed) ?? ""
  }
  var bedsValue: String {
    return (try? beds.value().trimmed) ?? ""
  }
  var rentValue: String {
    return (try? rent.value().trimmed) ?? ""
  }
  
}
