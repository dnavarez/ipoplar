//
//  NewPropertyController.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/25/22.
//

import UIKit

import NSObject_Rx
import RxCocoa
import RxSwift
import SVProgressHUD

class NewPropertyController: UIViewController {
  
  // Public Properties
  var viewModel: NewPropertyControllerViewModelProtocol!
  
  // IBOutlet
  @IBOutlet weak var containerView: UIView!
  
  @IBOutlet weak var addressTextField: UITextField!
  @IBOutlet weak var bathsTextField: UITextField!
  @IBOutlet weak var bedsTextField: UITextField!
  @IBOutlet weak var rentTextField: UITextField!
  
  @IBOutlet weak var submitButton: UIButton!
  
  // Private Properties
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    view.backgroundColor = .init(white: 0, alpha: 0.3)
  }
  
}

// MARK: - Setups

private extension NewPropertyController {
  
  func setup() {
    view.backgroundColor = .clear
    view.isOpaque = false
    
    containerView.layer.cornerRadius = 10
    
    setupTextFields()
    setupBindings()
    updateTextFields()
  }
  
  func setupTextFields() {
    rentTextField.keyboardType = .decimalPad
  }
  
  func setupBindings() {
    // Bind our textfield to our view model that will hold the values
    //
    addressTextField.rxBindTextField(to: viewModel.address).disposed(by: rx.disposeBag)
    bathsTextField.rxBindTextField(to: viewModel.baths).disposed(by: rx.disposeBag)
    bedsTextField.rxBindTextField(to: viewModel.beds).disposed(by: rx.disposeBag)
    rentTextField.rxBindTextField(to: viewModel.rent).disposed(by: rx.disposeBag)
    
    // Bind our submit button to textfields and enable it only if all textfields has value
    //
    let addressIsValid = addressTextField.rx.text.orEmpty.map { $0.isEmpty == false}
    let bathsIsValid = bathsTextField.rx.text.orEmpty.map { $0.isEmpty == false}
    let bedsIsValid = bedsTextField.rx.text.orEmpty.map { $0.isEmpty == false}
    let rentIsValid = rentTextField.rx.text.orEmpty.map { $0.isEmpty == false}
    
    Observable
      .combineLatest(
        addressIsValid, bathsIsValid, bedsIsValid, rentIsValid
      ) { $0 && $1 && $2 && $3 }
      .share(replay: 1)
      .bind(to: submitButton.rx.isEnabled)
      .disposed(by: rx.disposeBag)
  }
  
  func updateTextFields() {
    addressTextField.placeholder = viewModel.addressPlaceHolder
    bathsTextField.placeholder = viewModel.bathsPlaceHolder
    bedsTextField.placeholder = viewModel.bedsPlaceHolder
    rentTextField.placeholder = viewModel.rentPlaceHolder
  }
  
}

// MARK: - Events

private extension NewPropertyController {
  
  @IBAction func cancelButtonTapped(_ sender: Any) {
    view.backgroundColor = .clear
    dismiss(animated: true)
  }
  
  @IBAction func submitButtonTapped(_ sender: Any) {
    SVProgressHUD.show()
    viewModel.postNewProperty { [weak self] in
      SVProgressHUD.dismiss()
      self?.dismiss(animated: true)
    } failure: { error in
      SVProgressHUD.showError(withStatus: error?.localizedDescription)
    }
  }
  
}
