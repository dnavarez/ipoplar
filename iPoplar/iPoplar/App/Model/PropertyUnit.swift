//
//  PropertyUnit.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/25/22.
//

import Foundation

struct PropertyUnit: Codable {
  
  let buildingId: String
  let address: String
  let imageUrl: String
  let baths: Int
  let beds: Int
  let rent: Int
  
}
