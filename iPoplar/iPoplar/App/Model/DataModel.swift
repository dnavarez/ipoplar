//
//  DataModel.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/25/22.
//

import Foundation

struct DataModel: Codable {
  
  let buildings: [PropertyUnit]
  
}
