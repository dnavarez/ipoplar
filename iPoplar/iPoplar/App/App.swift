//
//  App.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/25/22.
//
//  This is our main application object.
//  This holds instances of all the services available in the app like the APIClient, SessionService, etc.
//  It also holds like the curtent logged-in user, and others things pertaining to the overall shared
//  status of the app.
//

import Foundation

class App {
  
  static let shared = App()
  
}

// MARK: - App Info

extension App {
  
  static var bundleIdentifier: String? { Bundle.main.bundleIdentifier }

  /// A dictionary, constructed from the bundle’s Info.plist file.
  static var info: [String: Any] { Bundle.main.infoDictionary ?? [:] }

  static var displayName: String { (info["CFBundleDisplayName"] as? String) ?? "iPoplar" }

  /// Alias for `CFBundleShortVersionString`.
  static var releaseVersion: String { (info["CFBundleShortVersionString"] as? String) ?? "1.0" }

  /// Alias for `CFBundleVersion`.
  static var buildNumber: String { (info["CFBundleVersion"] as? String) ?? "1" }
  
  static var osVersion: String {
    let version = ProcessInfo.processInfo.operatingSystemVersion
    return "\(version.majorVersion).\(version.minorVersion).\(version.patchVersion)"
  }
  
}
