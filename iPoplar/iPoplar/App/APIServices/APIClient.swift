//
//  APIClient.swift
//  FindingFalcone
//
//  Created by Dan Navarez on 1/25/22.
//

import Alamofire
//import TinyGraphQL


class APIClient: APIProtocol {
  
  func getBuildings(
    success: @escaping ([PropertyUnit]) -> Void,
    failure: @escaping failure
  ) {
    let query = Query("buildings", [:]) {
      "buildingId"
      "address"
      "imageUrl"
      "baths"
      "beds"
      "rent"
    }
    
    let reqURL = graphQL.request(for: query)
    
    request(
      urlRequest: reqURL,
      success: { response in
        if let buildings: [PropertyUnit] = response.decodedValue(forKeyPath: "buildings") {
          success(buildings)
        } else {
          failure(APIClientError.unknown)
        }
      },
      failure: failure
    )
  }
  
  /// Add New Property Unit
  ///
  func postNewPropertyUnit(
    address: String,
    baths: String,
    beds: String,
    rent: String,
    success: @escaping () -> Void,
    failure: @escaping failure
  ) {
    let newPropertyMutation = Mutation(
      "addBuilding", [
        "address": address,
        "baths": baths,
        "beds": beds,
        "rent": rent
      ]
    ){
      "address"
      "baths"
      "beds"
      "rent"
    }
    
    let reqURL = graphQL.request(for: newPropertyMutation)

    request(
      urlRequest: reqURL,
      success: { response in
        guard let _ = response.errors else {
          return success()
        }
        failure(APIClientError.unknown)
      },
      failure: failure
    )
  }
  
}
