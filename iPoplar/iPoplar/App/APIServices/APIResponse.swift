//
//  APIResponse.swift
//  iPoplar
//
//  Created by Dan Navarez on 5/25/22.
//

import Foundation

/// An object representation of the server's JSON response.
///
public struct APIResponse {
  
  /// Intentionally set to Optional-Any as it could be of any type or just be nil.
  /// It's up to the call site to determine the exact type of its value.
  /// If it's a Decodable type, use the method `decodedValue(forKeyPath:decoder:)`.
  ///
  let data: Any?
  
  let errors: [String: [String]]?
  
}

extension APIResponse: Decodable {
  
  enum CodingKeys: String, CodingKey {
    case data, errors
  }
  
  public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    data = (try container.decodeIfPresent(AnyDecodable.self, forKey: .data))?.value
    errors = try container.decodeIfPresent([String: [String]].self, forKey: .errors)
  }
  
}

// MARK: - Helpers

extension APIResponse {
  
  func decodedValue<T>(forKeyPath: String? = nil, decoder: JSONDecoder? = nil) -> T? where T: Decodable {
    guard var payload = data else { return nil }

    if let keyPath = forKeyPath {
      guard let d = nestedData(keyPath) else { return nil }
      payload = d
    }
    
    guard JSONSerialization.isValidJSONObject(payload) else {
      guard let val = payload as? T else { return nil }
      return val
    }
    
    do {
      let decoder = JSONDecoder()
      let json = try JSONSerialization.data(withJSONObject: payload)
      return try decoder.decode(T.self, from: json)
    } catch {
      print(error)
      return nil
    }
  }
  
  /// Returns the data at the given `keyPath`. Nil if path doesn't exist.
  ///
  private func nestedData(_ keyPath: String) -> Any? {
    guard let payload = data, !keyPath.isEmpty else { return nil }
    guard let dict = payload as? [String: Any] else { return nil }
    return dict[keyPath]
  }

}

