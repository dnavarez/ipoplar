//
//  APIProtocol.swift
//  FindingFalcone
//
//  Created by Dan Navarez on 1/25/22.
//

import Foundation
import Alamofire
//import TinyGraphQL

typealias success = ((APIResponse) -> Void)
typealias failure = ((Error?) -> Void)

fileprivate let GRAPH_API_URL   = "https://test-service-dev.cerberuslink.net/"
fileprivate let AUTHORIZATION   = "b13f3a77edc38e87e7427608a70485fb"

// MARK: - APIClientError

// TODO: Add conformance to CustomNSError.
enum APIClientError: Error {

  /// Indicates that a non-optional value of the given type was expected,
  /// but a null value was found.
  ///
  case dataNotFound(_ expectedType: Any.Type)
  
  case failedRequest
  
  case unknown
  
}

// MARK: - APIProtocol

protocol APIProtocol {}

extension APIProtocol {
  
  func request(
    urlRequest: URLRequest,
    success: @escaping success,
    failure: @escaping failure
  ) {
    Alamofire
      .request(urlRequest)
      .validate()
      .responseData { (response: DataResponse<Data>) in
        guard
          response.result.error == nil,
          let responseData = response.value
        else { return failure(response.result.error) }
        
        do {
          let resp = try JSONDecoder().decode(
            APIResponse.self,
            from: utf8Data(from: responseData)
          )
          success(resp)
        } catch {
          print(error)
        }
      }
  }
  
}

// MARK: Getter

extension APIProtocol {
  
  var graphQL: GraphQL {
    GraphQL(
      url: URL(string: GRAPH_API_URL)!,
      headers: headers
    )
  }
  
  var headers: [String: String] {
    [
      "Content-Type": "application/json",
      "Authorization": AUTHORIZATION
    ]
  }
  
}

// MARK: Helpers

extension APIProtocol {
  
  // TODO Throw error
  private func utf8Data(from data: Data) -> Data {
    let encoding = detectEncoding(of: data)
    guard encoding != .utf8 else { return data }
    guard let responseString = String(data: data, encoding: encoding) else {
      preconditionFailure("Could not convert data to string with encoding \(encoding.rawValue)")
    }
    guard let utf8Data = responseString.data(using: .utf8) else {
      preconditionFailure("Could not convert data to UTF-8 format")
    }
    return utf8Data
  }
  
  private func detectEncoding(of data: Data) -> String.Encoding {
    var convertedString: NSString?
    let encoding = NSString.stringEncoding(
      for: data,
      encodingOptions: nil,
      convertedString: &convertedString,
      usedLossyConversion: nil
    )
    return String.Encoding(rawValue: encoding)
  }
  
}

extension DataResponse {
  
  func decodeValue<T>() -> T? where T: Decodable {
    guard let payload = self.data else { return nil }
    
    do {
      let decoder = JSONDecoder()
      return try decoder.decode(T.self, from: payload)
    } catch {
      print(error)
      return nil
    }
  }
  
}
